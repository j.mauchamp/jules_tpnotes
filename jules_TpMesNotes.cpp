#include <iostream>
#include <string>
using namespace std;

int main()
{
	double note(0);

	while (note != -1)
	{
		cout << "Rentrez une note comprise entre 0 et 20" << endl;
		cin >> note;
		cout << "La note rentree est : " << note << endl;

		if (note < -1 || note > 20)
		{
			cout << "Veuillez rentrer une note comprise entre 0 et 20 ou rentrer -1 pour sortir" << endl;
		}
	}
}
